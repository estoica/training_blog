<?php

namespace BlogBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use BlogBundle\Validator\Constraints as CustomAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class Post
 *
 * @package BlogBundle\Entity
 *
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\PostRepository")
 */
class Post
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\Type("string")
     * @Assert\Regex("/^[A-Z]+.*$/", message="Post title must start with a capital letter")
     * @CustomAssert\ContainsAlphanumeric
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\Type("string")
     */
    protected $content;

    /**
     * @ORM\ManyToMany(targetEntity="Category", mappedBy="posts", cascade={"persist"})
     * @Assert\Valid()
     */
    protected $categories;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="post", cascade={"all"})
     */
    protected $comments;

    /**
     * Post constructor.
     */
    public function __construct()
    {
        $this->comments   = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }


    /**
     * @param Category $category
     *
     * @return $this
     */
    public function addCategory(Category $category)
    {
        $this->categories->add($category);
        $category->addPost($this);

        return $this;
    }

    /**
     * @param Category $category
     *
     * @return $this
     */
    public function removeCategory(Category $category)
    {
        $this->categories->removeElement($category);

        return $this;
    }

    /**
     * @return mixed
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param Comment $comment
     *
     * @return $this
     */
    public function addComment(Comment $comment)
    {
        $this->comments->add($comment);
        $comment->setPost($this);

        return $this;
    }

    /**
     * @param Comment $comment
     *
     * @return $this
     */
    public function removeComment(Comment $comment)
    {
        $this->comments->removeElement($comment);

        return $this;
    }

    public function setFeedItemDescription($description)
    {
       $this->content = $description;
    }

    public function setFeedItemLink($link)
    {
        // TODO: Implement setFeedItemLink() method.
    }

    public function setFeedItemPubDate(\DateTime $date)
    {
        // TODO: Implement setFeedItemPubDate() method.
    }

    public function setFeedItemTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @Assert\Callback
     */
    public function isTitleHacked(ExecutionContextInterface $context)
    {
        if ($this->getTitle() === 'Hackerman') {
            $context->buildViolation('h4x0r detected!')
                    ->atPath('title')
                    ->addViolation();
        }
    }
}
