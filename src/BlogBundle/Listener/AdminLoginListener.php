<?php

namespace BlogBundle\Listener;

use BlogBundle\Entity\AuthenticationLog;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;

class AdminLoginListener implements EventSubscriberInterface
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    public function __construct(EntityManager $em,
                                TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_SUCCESS => ['onAuthenticationSuccess', 10],
            AuthenticationEvents::AUTHENTICATION_FAILURE => ['onAuthenticationFailure']
        ];
    }

    public function onAuthenticationSuccess(AuthenticationEvent $event)
    {
        $authenticationLog = new AuthenticationLog();
        $authenticationLog->setUsername($event->getAuthenticationToken()->getUsername());
        $authenticationLog->setLoginDate(new \DateTime());

        $this->em->persist($authenticationLog);
        $this->em->flush();
    }
    
    public function onAuthenticationFailure(AuthenticationEvent $event)
    {
        
    }
}
