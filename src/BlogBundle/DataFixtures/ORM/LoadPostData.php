<?php

namespace BlogBundle\DataFixtures\ORM;

use BlogBundle\Entity\Category;
use BlogBundle\Entity\Comment;
use BlogBundle\Entity\Post;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadBlogData
 *
 * @package BlogBundle\DataFixtures\ORM
 */
class LoadPostData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $post1 = new Post();
        $post1->setTitle('Din lac în puţ! Roşia Montană a scăpat de cianură, dar acolo se va deschide o fabrică Frutti Fresh');
        $post1->setContent('O să le fie dor de lacul ăla imens cu cianuri! Deşi a scăpat ca prin urechile acului de o exploatare minieră care putea să otrăvească o emisferă întreagă, Roşia Montană se confruntă acum cu o criză ecologică şi mai mare: producător...');
        $post1->addCategory($this->getReference('city_category'));
        $post1->addCategory($this->getReference('soda_category'));
        $post1->addComment($this->getReference('comment1'));

        $post2 = new Post();
        $post2->setTitle('Pe DN1 va fi făcută o linie de tramvai, ca să poată şmecherii să se bage în faţă');
        $post2->setContent('În sfârşit, autorităţile se gândesc şi la românii adevăraţi, cu mândrele lor BMW-uri albe! Pentru a le asigura accesul facil pe Valea Prahovei cocalarilor patentaţi, CNADNR va amenaja o linie de tramvai pe DN1, astfel încât orice şmecher, barosan sau boss va putea fenta ambuteiajul până dincolo de Comarnic.');
        $post2->addCategory($this->getReference('jokes_category'));
        $post2->addComment($this->getReference('comment2'));
        $post2->addComment($this->getReference('comment3'));

        $manager->persist($post1);
        $manager->persist($post2);

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
