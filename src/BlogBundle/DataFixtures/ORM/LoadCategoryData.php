<?php

namespace BlogBundle\DataFixtures\ORM;

use BlogBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadBlogData
 *
 * @package BlogBundle\DataFixtures\ORM
 */
class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setName('City Category');
        
        $category2 = new Category();
        $category2->setName('Soda Category');
        
        $category3 = new Category();
        $category3->setName('Jokes Category');

        $manager->persist($category1);
        $manager->persist($category1);
        $manager->persist($category1);

        $manager->flush();

        $this->addReference('city_category', $category1);
        $this->addReference('soda_category', $category2);
        $this->addReference('jokes_category', $category3);
    }

    public function getOrder()
    {
        return 1;
    }
}
