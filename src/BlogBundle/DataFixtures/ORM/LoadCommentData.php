<?php

namespace BlogBundle\DataFixtures\ORM;

use BlogBundle\Entity\Category;
use BlogBundle\Entity\Comment;
use BlogBundle\Entity\Post;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadBlogData
 *
 * @package BlogBundle\DataFixtures\ORM
 */
class LoadCommentData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $comment1 = new Comment();
        $comment1->setAuthorEmail('cicibici@gmail.com');
        $comment1->setAuthorName('Unicul cici bici');
        $comment1->setContent('test comment');

        $comment2 = new Comment();
        $comment2->setAuthorEmail('raz@gmail.com');
        $comment2->setAuthorName('Unicul raz');
        $comment2->setContent('test comment raz');

        $comment3 = new Comment();
        $comment3->setAuthorEmail('eugen@gmail.com');
        $comment3->setAuthorName('Unicul eugen');
        $comment3->setContent('test comment eugen');

        $manager->persist($comment1);
        $manager->persist($comment2);
        $manager->persist($comment3);

        $manager->flush();

        $this->addReference('comment1', $comment1);
        $this->addReference('comment2', $comment2);
        $this->addReference('comment3', $comment3);
    }

    public function getOrder()
    {
        return 2;
    }
}
