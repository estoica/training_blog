<?php

namespace BlogBundle\Command;

use BlogBundle\Entity\Post;
use Doctrine\ORM\EntityManager;
use Eko\FeedBundle\Feed\Feed;
use Eko\FeedBundle\Feed\Reader;
use Eko\FeedBundle\Hydrator\DefaultHydrator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PopulatePostsCommand extends ContainerAwareCommand
{
    CONST FEED_URL = "http://feeds.bbci.co.uk/news/world/rss.xml";
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Reader
     */
    protected $feedReader;

    public function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('posts:populate')
            // the short description shown while running "php bin/console list"
            ->setDescription('Populate the posts')
            //argument
            ->addArgument('postsNumber', InputArgument::OPTIONAL, 'The number of posts to get', 5)
            // the full command description shown when running the command with the --help option
            ->setHelp("This command allows you to populate the posts table with the newest posts");
    }

    public function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $this->feedReader = $this->getContainer()->get('eko_feed.feed.reader');
        $this->feedReader->setHydrator(new DefaultHydrator());
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Start');
        $output->writeln(sprintf('Getting the feed from: %s', self::FEED_URL));
        $items = $this->feedReader->load(self::FEED_URL)->populate('BlogBundle\Entity\Post');
        $output->writeln('Feed retrieved successfully');

        /**
         * @var $item Post
         */
        $output->writeln('Adding category data');

        foreach ($items as $item)
        {
            $item->addCategory($this->em->getReference('BlogBundle:Category', 3));
            $this->em->persist($item);
        }

        $output->writeln('Flushing to database');
        $this->em->flush();
        $output->writeln('Done!');
    }
}