<?php

namespace BlogBundle\Services;

use BlogBundle\Entity\Post;
use Doctrine\ORM\EntityManager;

class PostDecoratorService
{
    CONST MAX_COMMENT_COUNT = 1;
    /**
     * @var EntityManager
     */
    private $em;
    private $mandatoryString;
//    /**
//     * @var \Swift_Mailer
//     */
//    private $optionalMailer;

    public function __construct(EntityManager $em,
                                $postMandatoryString)
    {
        $this->em = $em;
        $this->mandatoryString = $postMandatoryString;
    }

    public function getPostsWithCiciBici()
    {
        $posts = $this->em->getRepository(Post::class)->findAllAsArray();

        foreach ($posts as &$post){
            $post = $this->decoratePostWithCiciBici($post);
        }

        return $posts;
    }

//    public function setMailer($optionalDependency)
//    {
//        $this->optionalMailer = $optionalDependency;
//    }

    private function decoratePostWithCiciBici($post)
    {
        $post['cici'] = 'bici';
        if (count($post['comments']) > self::MAX_COMMENT_COUNT) {
            $post['cici'] = 'rici';
        } else {
            $post['mandatoryString'] = $this->mandatoryString;
        }

        return $post;
    }



}