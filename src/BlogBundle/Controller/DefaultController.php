<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        return $this->render('BlogBundle:Default:index.html.twig', [
            'posts' => $this->get('post.decorator_service')->getPostsWithCiciBici(),
        ]);

//        return $this->render('BlogBundle:Default:index.html.twig', [
//            'posts' => $this->getDoctrine()->getRepository(Post::class)->findAllOrderedByTitle(),
//        ]);
    }
}
