<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Category;
use BlogBundle\Entity\Post;
use BlogBundle\Form\Type\PostType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/post", name="homepage")
 */
class PostController extends Controller
{
    /**
     * @Route("/{id}", requirements={"id": "\d+"}, name="blog_bundle_post_details")
     * @param int $id
     *
     * @return Response
     */
    public function detailsAction($id)
    {
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);

        if ($post === null) {
            throw $this->createNotFoundException(sprintf('Post with id %s doesn\'t exists!', $id));
        }

        return $this->render('BlogBundle:Default:details.html.twig', [
            'post' => $this->getDoctrine()->getRepository(Post::class)->find($id),
        ]);
    }

    /**
     * @Route("/create", name="blog_bundle_post_create")
     * @var Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $post = new Post();

        // default values for a new post
//        $post->setTitle('default title');
//        $post->setContent('default content');
//        $post->addCategory((new Category())->setName('default category 1'));
//        $post->addCategory((new Category())->setName('default category 2'));

        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();

            $this->getDoctrine()->getEntityManager()->persist($post);
            $this->getDoctrine()->getEntityManager()->flush();

            $this->addFlash('notice', sprintf('Post with id %s was created!', $post->getId()));

            return $this->redirectToRoute('homepage');
        }

        return $this->render('@Blog/Default/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="blog_bundle_post_edit")
     * @var Request $request
     * @var int     $id
     *
     * @return Response
     */
    public function editAction(Request $request, $id)
    {
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);

        if ($post === null) {
            $this->createNotFoundException('Post not found');
        }

        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = $form->getData();

            $this->getDoctrine()->getEntityManager()->persist($post);
            $this->getDoctrine()->getEntityManager()->flush();

            $this->addFlash('notice', sprintf('Post with id %s was edited!', $post->getId()));

            return $this->redirectToRoute('homepage');
        }

        return $this->render('@Blog/Default/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="blog_bundle_post_delete")
     * @param int $id
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $post = $this->getDoctrine()->getRepository(Post::class)->find($id);

        if ($post === null) {
            throw $this->createNotFoundException(sprintf('Post with id %s doesn\'t exists!', $id));
        }

        $this->getDoctrine()->getManager()->remove($post);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('notice', sprintf('Post with id %s was deleted!', $id));

        return $this->redirectToRoute('homepage');
    }
}
