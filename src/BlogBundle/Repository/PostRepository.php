<?php

namespace BlogBundle\Repository;

use BlogBundle\Entity\Post;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class PostRepository
 *
 * @package BlogBundle\Repository
 */
class PostRepository extends EntityRepository
{
    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
//        return $this->findBy([], ['title' => 'DESC']);
        return parent::findAll();
    }

    /**
     * Query custom - using built-in methods
     * @param string $direction
     *
     * @return Post[]
     */
    public function findAllOrderedByTitle($direction = 'ASC')
    {
        return $this->findBy([], ['title' => $direction]);
    }

    /**
     * Query custom - using query builder abstract syntax over SQL platforms)
     * @param int $start Min title length
     * @param int $end   Max title length
     *
     * @return Post[]
     */
    public function findAllByTitleLengthRange($start, $end)
    {
        return $this->createQueryBuilder('p')
                    ->where('length(p.title) >= :start')
                    ->andWhere('length(p.title) <= :end')
                    ->setParameter('start', $start)
                    ->setParameter('end', $end)
                    ->getQuery()
                    ->getResult();
    }

    /**
     * Query custom - using DQL (abstract syntax over SQL platforms)
     * @param int $start Min title length
     * @param int $end   Max title length
     *
     * @return Post[]
     */
    public function findAllByTitleLengthRange2($start, $end)
    {
        return $this->getEntityManager()
                    ->createQuery('SELECT p from BlogBundle:Post p WHERE length(p.title) > :start AND length(p.title) <= :end')
                    ->setParameter('start', $start)
                    ->setParameter('end', $end)
                    ->getResult();
    }

    /**
     * Query custom - using native MySQL (will work only on MySQl - no shit?)
     * @param int $start Min title length
     * @param int $end   Max title length
     *
     * @return Post[]
     */
    public function findAllByTitleLengthRange3($start, $end)
    {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata('BlogBundle\Entity\Post', 'p');

        return $this->getEntityManager()
                    ->createNativeQuery('SELECT * from post p WHERE LENGTH(p.title) > ? AND LENGTH(p.title) <= ?', $rsm)
                    ->setParameter(1, $start)
                    ->setParameter(2, $end)
                    ->getResult();
    }

    public function findAllAsArray()
    {
        return $this->createQueryBuilder('p')
            ->select('p', 'cat', 'comm')
            ->leftJoin("p.categories", "cat")
            ->leftJoin("p.comments", "comm")
            ->getQuery()
            ->getArrayResult();
    }
}
